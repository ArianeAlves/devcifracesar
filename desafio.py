import hashlib
import requests
import json

#Request para o consumo de informações de uma api
req = requests.get('https://api.codenation.dev/v1/challenge/dev-ps/generate-data?token=d9aa4efd49cb3b52c66e2ae044e4047c0ce02a5c')
jsonReq = json.loads(req.text)

#A API nos retorna um json com um Texto cifrado e o numero de casas
numero_casas = jsonReq["numero_casas"]
cifrado = jsonReq["cifrado"]
decifrado=""
print(req.text+"\n")
print("Casas "+str(numero_casas))
print(cifrado)

#Iniciando a cifra de Júlio Cézar
alfabeto = ["a","b","c","d","e","f","g","h","i","j","k","l",
"m","n","o","p","q","r","s","t","u","v","w","x","y","z"]

#Passo o texto cifrado em um loop
for x in cifrado.lower():
	#Verifico se o caracter em "x" é pertencente ao alfabeto
	if x in alfabeto:
		#Capturo o index de "x" no alfabeto e subtraio com o numero de casas
		#Salvando esse resultado em outra variavel
		letra = alfabeto.index(x)-numero_casas
		#Pego o resultado obtdo anteriormente e o atribuo como um index do alfabeto
		#capturando o letra do alfabeto daquele index, assim concactenando a nova letra na variável decifrado
		decifrado += alfabeto[letra]
	else:
		#Caso não seja um caracter do alfabeto, "x" é concatenado na variavel decifrado
		decifrado += x

#Armazeno o texto claro no json na chave "decifrado"
print(decifrado)
jsonReq["decifrado"] = decifrado

# print(json.dumps(jsonReq)+"\n")

#Realizando o hash do texto claro e obtendo o seu resumo
hash_texto = hashlib.sha1(decifrado.encode('utf-8'))
resumo = hash_texto.hexdigest()

#Salvando o resumo no json
jsonReq["resumo_criptografico"] = resumo

print(json.dumps(jsonReq)+"\n")

#Salvando o json em um aquivo texto
with open("answer.json", "w") as file:

	file.write(json.dumps(jsonReq, indent=4))
	file.close()

#Criando uma estrura em json para o envio do arquivo em post
myfiles = {'answer' : open("answer.json" ,'rb')}

#Envio do arquivo em uma URL
response = requests.post("https://api.codenation.dev/v1/challenge/dev-ps/submit-solution?token=d9aa4efd49cb3b52c66e2ae044e4047c0ce02a5c"
	, files = myfiles )

#Response com informações do envio, vizualizando o Código do status
response.status_code
print(response.text)